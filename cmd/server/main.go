package main

import "fmt"

func run() error {
	fmt.Println("Starting up the application")
	return nil
}

func main() {
	fmt.Println("Hello Welcome to production level API development")

	if err := run(); err != nil {
		fmt.Println(err)
	}
}
